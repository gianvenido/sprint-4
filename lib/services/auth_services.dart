import 'package:airplane/model/user_model.dart';
import 'package:airplane/services/user_services.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthServices {
  FirebaseAuth _auth = FirebaseAuth.instance;

  //parameter yang dibutuhkan untuk Sign In
  Future<UserModel> signIn(
      {required String email, required String password}) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);

      UserModel user =
          await UserService().getUserById(userCredential.user!.uid);

      return user;
    } catch (e) {
      throw e;
    }
  }

  //parameter yang dibutuhkan untuk SignUp
  Future<UserModel> signup({
    required String email,
    required String password,
    required String name,
    String hobby = '',
  }) async {
    try {
      // register ke Firebase authentication
      UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);

      UserModel user = UserModel(
          id: userCredential.user!.uid,
          email: email,
          name: name,
          hobby: hobby,
          balance: 15200000);

      // register ke Firestore
      await UserService().setUser(user);

      return user;
    } catch (error) {
      throw error;
    }
  }

  //parameter yang dibutuhkan untuk Sign Out
  Future<void> signOut() async {
    try {
      await _auth.signOut();
    } catch (e) {
      throw e;
    }
  }
}
