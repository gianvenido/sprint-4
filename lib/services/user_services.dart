import 'package:airplane/model/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserService {
  //referensi kepada collection database di firestore
  CollectionReference _userReference =
      FirebaseFirestore.instance.collection('users');

  // Membuat method set User
  Future<void> setUser(UserModel user) async {
    try {
      _userReference.doc(user.id).set({
        'email': user.email,
        'name': user.name,
        'hobby': user.hobby,
        'balance': user.balance
      });
    } catch (error) {
      throw error;
    }
  }

  //Mengembalikan user model untuk mendapatkan data dari firestore
  Future<UserModel> getUserById(String id) async {
    try {
      DocumentSnapshot snapshot = await _userReference.doc(id).get();
      return UserModel(
          id: id,
          email: snapshot['email'],
          name: snapshot['name'],
          hobby: snapshot['hobby'],
          balance: snapshot['balance']);
    } catch (e) {
      throw e;
    }
  }
}
