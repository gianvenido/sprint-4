import 'package:airplane/model/destination_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DestinationService {
  CollectionReference _destinationRef =
      FirebaseFirestore.instance.collection('destinations');

  //Method
  Future<List<DestinationModel>> fetchDestination() async {
    try {
      //Lihat kembalian get, ternyata berupa QuerySnapshot, masukkan dalam variabel result
      QuerySnapshot result = await _destinationRef.get();

      //Karena datanya banyak, bisa dimasukkan didalam List
      //Assign result.doc.map.toList() pada variabel destinations,
      //data fromSJON dalam bentuk object harus diubah
      //dalam bentuk (as) Map<String, dynamic> agar tidak error
      List<DestinationModel> destinations = result.docs.map((e) {
        return DestinationModel.fromJSON(
            e.id, e.data() as Map<String, dynamic>);
      }).toList();

      return destinations;
    } catch (e, stackTrace) {
      print('Error: $e');
      print('StackTrace: ${stackTrace.toString()}');
      throw e;
    }
  }
}
