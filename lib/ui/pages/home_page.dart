import 'package:airplane/cubit/auth_cubit.dart';
import 'package:airplane/cubit/destination_cubit.dart';
import 'package:airplane/model/destination_model.dart';
import 'package:airplane/shared/theme.dart';
import 'package:airplane/ui/widgets/destination_card.dart';
import 'package:airplane/ui/widgets/destination_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //Buat sebuah method initState: dipanggil ketika pertamakali dibuka untuk fetching data
  @override
  void initState() {
    // TODO: implement initState
    context.read<DestinationCubit>().fetchDestinations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget header() {
      return BlocBuilder<AuthCubit, AuthState>(
        builder: (context, state) {
          if (state is AuthSuccess) {
            return Container(
              margin: EdgeInsets.only(
                  left: defaultMargin, right: defaultMargin, top: 30),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Howdy, \n${state.user.name}',
                          style: blackTextStyle.copyWith(
                              fontSize: 24, fontWeight: semiBold),
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 6),
                        Text(
                          'Where to fly today?',
                          style: greyTextStyle.copyWith(
                              fontSize: 16, fontWeight: light),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage('assets/img_profile.png'))))
                ],
              ),
            );
          } else {
            return SizedBox();
          }
        },
      );
    }

    Widget popularDestinations(List<DestinationModel> destinations) {
      return Container(
        margin: EdgeInsets.only(top: 30),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: destinations.map((DestinationModel destination) {
              return DestinationCard(destination);
            }).toList(),

            //Tidak dipakai lagi ketika sudah berhasil fetch data dari Firestore
            // DestinationCard(
            //   name: 'Musi River',
            //   location: 'Palembang',
            //   rating: 4.3,
            //   imgUrl: 'assets/img_destination1.png',
            // ),
            // DestinationCard(
            //   name: 'Gedung Djoeang 45',
            //   location: 'Surabaya',
            //   rating: 4.6,
            //   imgUrl: 'assets/img_destination2.png',
            // ),
            // DestinationCard(
            //   name: 'Kuta Mandalika',
            //   location: 'Lombok',
            //   rating: 4.8,
            //   imgUrl: 'assets/img_destination3.png',
            // ),
            // DestinationCard(
            //   name: 'Manarra',
            //   location: 'Semarang',
            //   rating: 4.2,
            //   imgUrl: 'assets/img_destination4.png',
            // ),
            // DestinationCard(
            //   name: 'Ambarukmo Heights',
            //   location: 'Yogyakarta',
            //   rating: 4.8,
            //   imgUrl: 'assets/img_destination5.png',
            // ),
          ),
        ),
      );
    }

    Widget newDestinations(List<DestinationModel> destinations) {
      return Container(
          margin: EdgeInsets.only(
              top: 30, left: defaultMargin, right: defaultMargin, bottom: 100),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'New This Year',
                style:
                    blackTextStyle.copyWith(fontSize: 18, fontWeight: semiBold),
              ),

              //Membuat destination tile

              Column(
                children: destinations.map((DestinationModel destination) {
                  return DestinationTile(destination);
                }).toList(),
              )
            ],
          ));
    }

    return BlocConsumer<DestinationCubit, DestinationState>(
      listener: (context, state) {
        // TODO: implement listener
        if (state is DestinationFailed) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(state.error),
              backgroundColor: redColor,
            ),
          );
        }
      },
      builder: (context, state) {
        if (state is DestinationSuccess) {
          return ListView(
            children: [
              header(),
              popularDestinations(state.destinations),
              newDestinations(state.destinations),
            ],
          );
        }

        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
