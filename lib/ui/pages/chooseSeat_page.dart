import 'package:airplane/cubit/seat_cubit.dart';
import 'package:airplane/model/destination_model.dart';
import 'package:airplane/model/transaction_model.dart';
import 'package:airplane/shared/theme.dart';
import 'package:airplane/ui/pages/checkout_page.dart';
import 'package:airplane/ui/widgets/customButton.dart';
import 'package:airplane/ui/widgets/seatItems.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class ChooseSeatPage extends StatelessWidget {
  final DestinationModel destination;

  const ChooseSeatPage(this.destination, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget title() {
      return Container(
        margin: EdgeInsets.only(top: 40),
        child: Text(
          'Select Your \nFavourite Seat',
          style: blackTextStyle.copyWith(fontSize: 24, fontWeight: semiBold),
        ),
      );
    }

    Widget seatStatus() {
      return Container(
        margin: EdgeInsets.only(top: 30),
        child: Row(
          children: [
            // NOTE: AVAILABLE
            Container(
              width: 16,
              height: 16,
              margin: EdgeInsets.only(right: 6),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/icon_available.png'),
                ),
              ),
            ),
            Text(
              'Available',
              style: blackTextStyle,
            ),
            // NOTE: SELECTED
            Container(
              width: 16,
              height: 16,
              margin: EdgeInsets.only(right: 6, left: 10),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/icon_selected.png'),
                ),
              ),
            ),
            Text(
              'Selected',
              style: blackTextStyle,
            ),
            // NOTE: UNAVAILABLE
            Container(
              width: 16,
              height: 16,
              margin: EdgeInsets.only(left: 10),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/icon_unavailable.png'),
                ),
              ),
            ),
            Text(
              'Unavailable',
              style: blackTextStyle,
            ),
          ],
        ),
      );
    }

    Widget selectSeat() {
      return BlocBuilder<SeatCubit, List<String>>(
        builder: (context, state) {
          return Container(
              margin: EdgeInsets.only(top: 30),
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: 22, vertical: 30),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(defaultRadius),
                color: whiteColor,
              ),
              child: Column(
                children: [
                  // NOTE: SEAT INDICATORS
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        width: 48,
                        height: 48,
                        child: Center(
                          child: Text(
                            'A',
                            style: greyTextStyle.copyWith(fontSize: 16),
                          ),
                        ),
                      ),
                      Container(
                        width: 48,
                        height: 48,
                        child: Center(
                          child: Text(
                            'B',
                            style: greyTextStyle.copyWith(fontSize: 16),
                          ),
                        ),
                      ),
                      Container(
                        width: 48,
                        height: 48,
                        child: Center(
                          child: Text(
                            '',
                            style: greyTextStyle.copyWith(fontSize: 16),
                          ),
                        ),
                      ),
                      Container(
                        width: 48,
                        height: 48,
                        child: Center(
                          child: Text(
                            'C',
                            style: greyTextStyle.copyWith(fontSize: 16),
                          ),
                        ),
                      ),
                      Container(
                        width: 48,
                        height: 48,
                        child: Center(
                          child: Text(
                            'D',
                            style: greyTextStyle.copyWith(fontSize: 16),
                          ),
                        ),
                      ),
                    ],
                  ),

                  // NOTE: SEAT 1
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SeatItems(id: 'A1', isAvailable: false),
                        SeatItems(id: 'B1', isAvailable: false),
                        Container(
                          width: 48,
                          height: 48,
                          child: Center(
                            child: Text(
                              '1',
                              style: greyTextStyle.copyWith(fontSize: 16),
                            ),
                          ),
                        ),
                        SeatItems(id: 'C1', isAvailable: false),
                        SeatItems(id: 'D1'),
                      ],
                    ),
                  ),

                  // NOTE: SEAT 2
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SeatItems(id: 'A2', isAvailable: false),
                        SeatItems(id: 'B2'),
                        Container(
                          width: 48,
                          height: 48,
                          child: Center(
                            child: Text(
                              '2',
                              style: greyTextStyle.copyWith(fontSize: 16),
                            ),
                          ),
                        ),
                        SeatItems(id: 'C2'),
                        SeatItems(id: 'D2', isAvailable: false),
                      ],
                    ),
                  ),

                  // NOTE: SEAT 3
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SeatItems(id: 'A3'),
                        SeatItems(id: 'B3'),
                        Container(
                          width: 48,
                          height: 48,
                          child: Center(
                            child: Text(
                              '3',
                              style: greyTextStyle.copyWith(fontSize: 16),
                            ),
                          ),
                        ),
                        SeatItems(id: 'C3'),
                        SeatItems(id: 'D3'),
                      ],
                    ),
                  ),

                  // NOTE: SEAT 4
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SeatItems(id: 'A4'),
                        SeatItems(id: 'B4'),
                        Container(
                          width: 48,
                          height: 48,
                          child: Center(
                            child: Text(
                              '4',
                              style: greyTextStyle.copyWith(fontSize: 16),
                            ),
                          ),
                        ),
                        SeatItems(id: 'C4'),
                        SeatItems(id: 'D4'),
                      ],
                    ),
                  ),
                  // NOTE: SEAT 4
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SeatItems(id: 'A5'),
                        SeatItems(id: 'B5'),
                        Container(
                          width: 48,
                          height: 48,
                          child: Center(
                            child: Text(
                              '5',
                              style: greyTextStyle.copyWith(fontSize: 16),
                            ),
                          ),
                        ),
                        SeatItems(id: 'C5'),
                        SeatItems(id: 'D5'),
                      ],
                    ),
                  ),

                  //NOTE: YOUR SEAT
                  Container(
                    margin: EdgeInsets.only(top: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Your seat',
                          style: greyTextStyle.copyWith(
                            fontWeight: light,
                            fontSize: 16,
                          ),
                        ),
                        Text(
                          state.join(', '),
                          style: blackTextStyle.copyWith(
                              fontWeight: medium,
                              fontSize: 16,
                              overflow: TextOverflow.ellipsis),
                        ),
                      ],
                    ),
                  ),

                  //NOTE: TOTAL
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Total',
                          style: greyTextStyle.copyWith(
                              fontWeight: light, fontSize: 16),
                        ),
                        Text(
                          NumberFormat.currency(
                                  locale: 'id',
                                  symbol: 'IDR ',
                                  decimalDigits: 0)
                              .format(state.length * destination.price),
                          style: purpleTextStyle.copyWith(
                              fontWeight: semiBold, fontSize: 16),
                        ),
                      ],
                    ),
                  ),
                ],
              ));
        },
      );
    }

    Widget checkOutButton() {
      return BlocBuilder<SeatCubit, List<String>>(
        builder: (context, state) {
          return CustomButton(
            titleTextButton: 'Continue to Checkout',
            onPressed: () {
              int price = destination.price * state.length;

              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CheckoutPage(
                    TransactionModel(
                      destination: destination,
                      pax: state.length,
                      selectedSeats: state.join(', '),
                      insurance: true,
                      refundable: false,
                      price: price,
                      vat: 0.45,
                      grandTotal: price + (price * 0.45).toInt(),
                    ),
                  ),
                ),
              );
            },
            margin: EdgeInsets.only(top: 30, bottom: 46),
          );
        },
      );
    }

    return Scaffold(
      backgroundColor: backgroundColor,
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        children: [
          title(),
          seatStatus(),
          selectSeat(),
          checkOutButton(),
        ],
      ),
    );
  }
}
