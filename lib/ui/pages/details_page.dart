import 'package:airplane/model/destination_model.dart';
import 'package:airplane/shared/theme.dart';
import 'package:airplane/ui/pages/chooseSeat_page.dart';
import 'package:airplane/ui/widgets/customButton.dart';
import 'package:airplane/ui/widgets/interestItems.dart';
import 'package:airplane/ui/widgets/photoItems.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DetailsPage extends StatelessWidget {
  final DestinationModel destination;

  const DetailsPage(this.destination, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget backgroundImage() {
      return Container(
        width: double.infinity,
        height: 450,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(destination.imgUrl),
          ),
        ),
      );
    }

    Widget customShadow() {
      return Container(
        height: 214,
        width: double.infinity,
        margin: EdgeInsets.only(top: 236),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Colors.black.withOpacity(0),
              Colors.black.withOpacity(0.75),
            ],
          ),
        ),
      );
    }

    Widget content() {
      return SingleChildScrollView(
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: Column(
            children: [
              // NOTE: EMBLEM
              Container(
                margin: EdgeInsets.only(top: 40),
                width: 108,
                height: 24,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/icon_emblem.png'),
                  ),
                ),
              ),

              // NOTE: TITLE
              Container(
                margin: EdgeInsets.only(top: 256),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(destination.name,
                              overflow: TextOverflow.ellipsis,
                              style: whiteTextStyle.copyWith(
                                  fontSize: 24, fontWeight: semiBold)),
                          Text(destination.location,
                              style: whiteTextStyle.copyWith(
                                  fontSize: 16, fontWeight: light)),
                        ],
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 24,
                          height: 24,
                          margin: EdgeInsets.only(right: 8),
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/icon_star.png'),
                            ),
                          ),
                        ),
                        Text(
                          '${destination.rating}',
                          style: whiteTextStyle.copyWith(
                              fontWeight: medium, fontSize: 20),
                        ),
                      ],
                    ),
                  ],
                ),
              ),

              // NOTE: DESCRIPTION
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 30),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                decoration: BoxDecoration(
                  color: whiteColor,
                  borderRadius: BorderRadius.circular(defaultRadius),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // NOTE: ABOUT
                    Text(
                      'About',
                      style: blackTextStyle.copyWith(
                          fontWeight: semiBold, fontSize: 16),
                    ),
                    SizedBox(height: 10),
                    Text(
                      'Saatnya bersantap kuliner hasil sungai di atas Warung Apung, lalu berwisata menyusuri Sungai Musi yang membelah Kota Palembang menjadi dua bagian, Ulu dan Ilir, serta menyaksikan panorama matahari tenggelam dari Pulau Kemaro yang berada tepat di tengah sungai.',
                      style: blackTextStyle.copyWith(
                        fontWeight: regular,
                        height: 2,
                      ),
                    ),
                    // NOTE: PHOTO
                    SizedBox(height: 30),
                    Text(
                      'Photos',
                      style: blackTextStyle.copyWith(
                          fontWeight: semiBold, fontSize: 16),
                    ),
                    SizedBox(height: 6),
                    Row(
                      children: [
                        PhotoItems(urlPhoto: 'assets/img_photo1.png'),
                        PhotoItems(urlPhoto: 'assets/img_photo2.png'),
                        PhotoItems(urlPhoto: 'assets/img_photo3.png'),
                      ],
                    ),
                    // NOTE: INTEREST
                    SizedBox(height: 30),
                    Text(
                      'Interest',
                      style: blackTextStyle.copyWith(
                          fontWeight: semiBold, fontSize: 16),
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        InterestItems(text: 'Diving Spot'),
                        InterestItems(text: 'Family Park'),
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      children: [
                        InterestItems(text: 'Ampera Bridge'),
                        InterestItems(text: 'Seafood Resto'),
                      ],
                    ),
                  ],
                ),
              ),

              // NOTE: PRICE & BOOK BUTTON
              Container(
                margin: EdgeInsets.symmetric(vertical: 30),
                width: double.infinity,
                child: Row(
                  children: [
                    // NOTE: PRICE
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            NumberFormat.currency(
                              locale: 'id',
                              symbol: 'IDR ',
                              decimalDigits: 0,
                            ).format(destination.price),
                            style: blackTextStyle.copyWith(
                                fontWeight: medium, fontSize: 18),
                          ),
                          SizedBox(height: 5),
                          Text(
                            'per orang',
                            style: greyTextStyle.copyWith(fontWeight: light),
                          ),
                        ],
                      ),
                    ),
                    // NOTE: BOOK BUTTON

                    CustomButton(
                      titleTextButton: 'Book Now',
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ChooseSeatPage(destination),
                            ));
                      },
                      width: 170,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      backgroundColor: backgroundColor,
      body: Stack(
        children: [
          backgroundImage(),
          customShadow(),
          content(),
        ],
      ),
    );
  }
}
