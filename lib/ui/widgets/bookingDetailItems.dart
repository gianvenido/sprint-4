import 'package:airplane/shared/theme.dart';
import 'package:flutter/material.dart';

class BookingDetailItems extends StatelessWidget {
  final String title;
  final String valueText;
  final Color valueColor;

  const BookingDetailItems(
      {Key? key,
      required this.title,
      required this.valueColor,
      required this.valueText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: 6),
            width: 16,
            height: 16,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/icon_checklist.png'),
              ),
            ),
          ),
          Text(title, style: blackTextStyle),
          Spacer(),
          Text(
            valueText,
            style: blackTextStyle.copyWith(
                fontWeight: semiBold, color: valueColor),
          ),
        ],
      ),
    );
  }
}
