import 'package:flutter/material.dart';
import '../../shared/theme.dart';

class CustomTextFormField extends StatelessWidget {
  final String title;
  final String hintText;
  final bool obscureText;
  final EdgeInsets margin;
  final TextEditingController controller;
  //Controller

  const CustomTextFormField({
    Key? key,
    required this.title,
    required this.hintText,
    this.obscureText = false,
    this.margin = EdgeInsets.zero,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: blackTextStyle.copyWith(fontSize: 14, fontWeight: regular),
          ),
          SizedBox(height: 6),
          TextFormField(
            controller: controller,
            obscureText: obscureText,
            style: blackTextStyle.copyWith(fontSize: 16, fontWeight: regular),
            cursorColor: blackColor,
            decoration: InputDecoration(
                hintText: hintText,
                hintStyle:
                    greyTextStyle.copyWith(fontSize: 14, fontWeight: regular),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(defaultRadius),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(defaultRadius),
                  borderSide: BorderSide(color: primaryColor),
                )),
          ),
        ],
      ),
    );
  }
}
