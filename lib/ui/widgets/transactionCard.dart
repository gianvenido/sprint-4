import 'package:airplane/model/transaction_model.dart';
import 'package:airplane/shared/theme.dart';
import 'package:airplane/ui/widgets/bookingDetailItems.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TransactionCard extends StatelessWidget {
  final TransactionModel transaction;

  const TransactionCard(this.transaction, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(defaultRadius),
        color: whiteColor,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //NOTE : DESTINATION TILE
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 70,
                height: 70,
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(defaultRadius),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(transaction.destination.imgUrl),
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      transaction.destination.name,
                      style: blackTextStyle.copyWith(
                          fontSize: 18, fontWeight: medium),
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(height: 5),
                    Text(
                      transaction.destination.location,
                      style: greyTextStyle.copyWith(fontWeight: light),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 18,
                    height: 18,
                    margin: EdgeInsets.only(right: 5),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/icon_star.png'),
                      ),
                    ),
                  ),
                  Text(
                    transaction.destination.rating.toString(),
                    style: blackTextStyle.copyWith(fontWeight: medium),
                  ),
                ],
              ),
            ],
          ),

          // NOTE: BOOKING DETAILS
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Text(
              'Booking Details',
              style:
                  blackTextStyle.copyWith(fontSize: 16, fontWeight: semiBold),
            ),
          ),

          // NOTE: Booking Details Item
          BookingDetailItems(
            title: 'Traveler',
            valueColor: blackColor,
            valueText: '${transaction.pax} person',
          ),
          BookingDetailItems(
            title: 'Seat',
            valueColor: blackColor,
            valueText: transaction.selectedSeats,
          ),
          BookingDetailItems(
            title: 'Insurance',
            valueColor: transaction.insurance ? greenColor : redColor,
            valueText: transaction.insurance ? 'YES' : 'NO',
          ),
          BookingDetailItems(
            title: 'Refundable',
            valueColor: transaction.refundable ? greenColor : redColor,
            valueText: transaction.refundable ? 'YES' : 'NO',
          ),
          BookingDetailItems(
            title: 'VAT',
            valueColor: blackColor,
            valueText: '${(transaction.vat * 100).toStringAsFixed(0)} %',
          ),
          BookingDetailItems(
            title: 'Price',
            valueColor: blackColor,
            valueText: NumberFormat.currency(
                    locale: 'id', symbol: 'IDR ', decimalDigits: 0)
                .format(transaction.price),
          ),
          BookingDetailItems(
            title: 'Grand Total',
            valueColor: primaryColor,
            valueText: NumberFormat.currency(
                    locale: 'id', symbol: 'IDR ', decimalDigits: 0)
                .format(transaction.grandTotal),
          ),
        ],
      ),
    );
  }
}
