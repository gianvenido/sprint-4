import 'package:airplane/cubit/seat_cubit.dart';
import 'package:airplane/shared/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SeatItems extends StatelessWidget {
  // NOTE: Status 0. Available, 1. Selected, 2. Unavailable

  // final int status;
  //status tidak digunakan lagi ketika ada isAvailable

  //tambahkan ID untuk baris seat
  final String id;
  final bool isAvailable;

  const SeatItems({
    Key? key,
    required this.id,
    this.isAvailable = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isSelected = context.watch<SeatCubit>().isSelected(id);

    backgroundColor() {
      if (!isAvailable) {
        return bgUnavailableColor;
      } else {
        if (isSelected) {
          return primaryColor;
        } else {
          return bgAvailableColor;
        }
      }
    }

    borderColor() {
      if (!isAvailable) {
        return bgUnavailableColor;
      } else {
        return primaryColor;
      }
    }

    child() {
      if (isSelected) {
        return Center(
          child: Text(
            'YOU',
            style: whiteTextStyle.copyWith(fontWeight: bold),
          ),
        );
      } else {
        return SizedBox();
      }
    }

    return GestureDetector(
      onTap: () {
        if (isAvailable) {
          context.read<SeatCubit>().selectSeat(id);
        }
      },
      child: Container(
        width: 48,
        height: 48,
        decoration: BoxDecoration(
          color: backgroundColor(),
          border: Border.all(
            color: borderColor(),
            width: 2,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
        child: child(),
      ),
    );
  }
}
