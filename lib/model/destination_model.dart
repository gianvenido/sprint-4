import 'package:equatable/equatable.dart';

class DestinationModel extends Equatable {
  final String id;
  final String name;
  final String location;
  final String imgUrl;
  final double rating;
  final int price;

  DestinationModel({
    required this.id,
    this.name = '',
    this.location = '',
    this.imgUrl = '',
    this.rating = 0.0,
    this.price = 0,
  });

  //Tambahkan method factory mengambil data dari firebase
  factory DestinationModel.fromJSON(String id, Map<String, dynamic> json) =>
      DestinationModel(
        id: id,
        name: json['name'],
        location: json['location'],
        imgUrl: json['imgUrl'],
        rating: json['rating'].toDouble(),
        price: json['price'],
      );

  //Method me
  Map<String, dynamic> toJSON() => {
        'id': id,
        'name': name,
        'location': location,
        'imgUrl': imgUrl,
        'rating': rating,
        'price': price,
      };

  @override
  // TODO: implement props
  List<Object?> get props => [id, name, location, imgUrl, rating, price];
}
