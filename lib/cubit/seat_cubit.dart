import 'package:bloc/bloc.dart';

class SeatCubit extends Cubit<List<String>> {
  SeatCubit() : super([]);

  //menambahkan method agar user klik masuk ke list
  void selectSeat(String id) {
    if (!isSelected(id)) {
      state.add(id);
    } else {
      state.remove(id);
    }
    emit(List.from(state));
  }

  //method untuk melihat apakah user sudah klik seat yang sama?
  bool isSelected(String id) {
    int index = state.indexOf(id);
    print('index: $index');

    if (index == -1) {
      return false;
    } else {
      return true;
    }
    ;
  }
}
